# iOSDeviceSupport
 
#### 为了解决这个问题：[Your Xcode version may be too old for your iOS version]，
#### 不想下载安装新版Xcode,安装新版Xcode 又依赖于新版MacOS系统，要更新的内容太多而且新系统不问题bug多多。


> 警告！！！【update 20240819】
 ** iOS 17+ 的可运行环境必须在Xcode 14+, 最好是Xcode 15+ **

有设备不可用时可尝试终端中运行以下命令：

```
defaults write com.apple.dt.Xcode DVTEnableCoreDevice enabled
```
“然后重新启动 Xcode 14，iOS 17 设备将如下所示显示在 Xcode 中，您可以像往常一样运行和调试应用进程。（您可能需要转到设备和模拟器来配对/信任设备）”
可能还需要Xcode15 beta 和 Xcode 14共存

内容详细来源：
https://forums.developer.apple.com/forums/thread/730947?answerId=758061022#758061022


 **添加DeviceSupport文件的方式，仅作临时Xcode版本不升级使用,长期使用建议升级官方最新版** 

大概有个规则(未经验证，自评版本)：
iOS 18.x 依赖于Xcode 15 以上才可以
iOS 17.x 依赖于Xcode 14.2 以上才可以
iOS 16.x 依赖于Xcode 14 以上才可以
两个系统得大版本对齐，比如iOS 15对应 Xcode 13,iOS 16对应 Xcode 14




>  Xcode 14.x 的版本实际上应该无法运行 iOS 17.x 系列的版本，必须升级到 Xcode 15.x 才可以，且 macOS 系统需要升级为 macOS Sonoma 才可以运行 Xcode 15.x；
> 另外一点就是，一旦升级到了 macOS Sonoma 版本的系统后，Xcode 14.x 将无法运行，不再像以前一样可以运行低版本的 Xcode 版本，请谨慎升级。
 `摘自 https://github.com/jijiucheng/JJC_iOS_DeviceSupport`

### 各个版本的iOS Device Support

### NEW  **添加最新支持版本 iOS 17.2正式版**

**稳定发行版本iOS 17.1.1 17.1 16.6 16.5 iOS 16.4 iOS 16.3 iOS 16.2, iOS 16.1.1, iOS 15.7** 

### iOS17 版本更新记录：

> iOS17.6_beta4（21G5075a）2024/07/16

> iOS17.5.1（21F90）2024/05/20

> iOS17.5（21F79）2024/05/13

> iOS17.2（21C62）2023/12/11

> iOS17.1.1（21B91）2023/11/07

> iOS17.1（21B74、21B80）2023/10/25

存放路径：`/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/DeviceSupport`


**详细使用步骤：**
> 0. 下载单个Device Support.zip的压缩文件，并解压
> 1. 复制👆🏻上面路径
> 2. `Shift-Command-G`：打开“前往文件夹”窗口
> 3. 复制已解压文件夹到当前路径的文件夹窗口，比如解压后文件夹名就是 [16.3],不用修改直接复制整个文件夹
> 4. 输入当前用户密码，复制完成后，退出重启Xcode即可使用新版iOS Device Support 🐮🍺

![具体路径](Imgs/DeveloperDiskImage.png)

GitHub下载速度慢的同学可以移步码云（Gitee）：https://gitee.com/qiu1993/iOSDeviceSupport


只是需要单个的可以下载单个，如下

## 便捷单包下载地址：（由新到旧排序）

* #### [iOS 17.2](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS17/17.2.zip)
* #### [iOS 17.1.1](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS17/17.1.1.zip)
* #### [iOS 17.1](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS17/17.1.zip)

* #### [iOS 16.6](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.6.zip)
* #### [iOS 16.5](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.5.zip)
* #### [iOS 16.4](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.4.zip)
* #### [iOS 16.4 beta](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.4(FromXcode_14.3_beta_2.xip).zip)
* #### [iOS 16.3](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.3.zip)
* #### [iOS 16.2](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.2.zip)
* #### [iOS 16.1](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.1.zip)
* #### [iOS 16.0](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.0.zip)

* #### [iOS 15.7](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.7.zip)
* #### [iOS 15.6](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.6.zip)
* #### [iOS 15.5](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.5.zip)
* #### [iOS 15.4](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.4.zip)
* #### [iOS 15.2](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.2.zip)
* #### [iOS 15.1](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.1.zip)
* #### [iOS 15.0](https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.0.zip)

## 可引用下载地址拼写规则如下：（由新到旧排序）

* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS17/17.2.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS17/17.1.1.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS17/17.1.zip

* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.6.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.5.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.4(FromXcode_14.3_beta_2.xip).zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.3.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.2.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.1.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS16/16.0.zip

* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.7.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.6.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.5.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.4.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.2.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.1.zip
* #### https://gitee.com/qiu1993/iOSDeviceSupport/raw/master/iOS15/15.0.zip

# 坚持维护更新中~

喜欢的点个star,方便下次再来^_^。

### 访问量

![](http://profile-counter.glitch.me/evilbutcher/count.svg)


